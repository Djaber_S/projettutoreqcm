<?php
	session_start();
	if($_SESSION['verifConnexion'] != 1){
		header("Location:index.php");
	}
	$nomQCM=$_SESSION['nomQuestionnaire'];
	$fichier=$_SESSION['fichierCSV'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
    <title>EASY TEST | RESULTATS</title>
    <link rel="stylesheet" href="style/style-pageUser.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>

    <header class="top">
        <nav class="navigation container">
            <a href="index.php" class="logo">EASY TEST</a>
            <ul class="nav-right">


				<form action='' method='post'>
					<input type="submit" id="bdeconnexion" name="deco" value="Déconnexion"/>
				</form>
						<li><a href="pageCompte.php">Compte</a></li>
						<li><a href="pageUtilisateur.php">Création QCM</a></li>
						 <li><a href="correction_nomQCM.php">Correction</a></li>
			</ul>
		</nav>
    </header>
	
<body>
	<center><h1>Résultats des étudiants</h1></center>
</body>
</html>

<?php
	require("parametres.php");
	//correction
	echo "<p>Le fichier resultats.csv contenant les résultats des étudiants a été créé dans le répertoire $dossierQCM"."$nomQCM. Voici les résultats des étudiants:</p><br/><br/>";
	$commande="sudo -u $utilisateur auto-multiple-choice prepare --mode b --prefix $dossierQCM"."$nomQCM   $dossierQCM"."$nomQCM/$nomQCM.tex "."--data $dossierQCM"."$nomQCM/data";
	exec($commande);
	//infos du layout
	$commande="sudo -u $utilisateur auto-multiple-choice meptex --src $dossierQCM"."$nomQCM/calage.xy --data $dossierQCM"."$nomQCM/data";
	exec($commande);
		//analyse des scans
	$commande="sudo -u $utilisateur auto-multiple-choice analyse --projet $dossierQCM"."$nomQCM $dossierQCM"."$nomQCM/scans/*";
	exec($commande);
	//calcul des notes
	$commande="sudo -u $utilisateur auto-multiple-choice note --data $dossierQCM"."$nomQCM/data --seuil 0.15";
	exec($commande);
	//association
	$commande="sudo -u $utilisateur auto-multiple-choice association-auto --data $dossierQCM"."$nomQCM/data --notes-id NumeroEtudiant --liste $fichier --liste-key NumeroEtudiant";
	exec($commande);
	//exportation du fichier csv contenant les résultats
	$commande="sudo -u $utilisateur auto-multiple-choice export --data $dossierQCM"."$nomQCM/data --module CSV --fich-noms $fichier --o $dossierQCM"."$nomQCM/resultats.csv";
	exec($commande);
	$commande="sudo -u $utilisateur chmod 777 $dossierQCM"."$nomQCM/resultats.csv";
	exec($commande);
	//affichage des notes
	echo"<center><table border = '1' cellpadding = '15'>";
		echo "<link href = 'style/style.css' rel = 'stylesheet' type = 'text/css' />";
		$row = 1;
		if(($handle = fopen($dossierQCM.$nomQCM."/resultats.csv",'r')) !== FALSE ){
			while(($pointeur = fgetcsv($handle,1024,',')) !== FALSE ){
			
				$num = count($pointeur);
				echo"<tr>";
				if($row == 1){
				
					for ($c=0; $c < $num; $c++) {
						if($c == 3){
							echo"<td>";
							echo "<B>Note</B>";
							echo "</td>";
						}
						else if($c == 2){
							echo"<td>";
							echo "<B> Nom </B>";
							echo "</td>";
							echo"<td>";
							echo "<B> Prénom </B>";
							echo "</td>";						
						}
					}
				}
				else {
				
					for ($c=0; $c < $num; $c++) {
						if($c == 3){
							if($c == 3 & $pointeur[$c] <10){
								echo"<td id = 'not_N'>";
								echo $pointeur[$c];
								echo "</td>";
							}
							else if($c == 3 & $pointeur[$c] >= 10){
								echo"<td id = 'not_P'>";
								echo $pointeur[$c];
								echo "</td>";	
							}
							else{
								echo"<td>";
								echo $pointeur[$c];
								echo "</td>";
							}
						}
						else if($c == 2){
							$test = $pointeur[$c];
							$array = explode(" ",$test);
							echo"<td>";
							echo $array[0];
							echo "</td>";
							echo"<td>";
							echo $array[1];
							echo "</td>";
						
						
						}
					}
				
				}
				echo "</tr>";
				$row++;
			}echo"</table></center>";
		}
?>

