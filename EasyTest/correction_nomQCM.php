<?php
	session_start();
	if($_SESSION['verifConnexion'] != 1){
		header("Location:index.php");
	}

	//on vérifie que le QCM à corriger existe
	if(isset($_POST['verif'])){
		include('fonctions.php');
		$bdd = bdd();
		$nomQCM =  str_replace(' ', '_', $_POST['nomQuestionnaire']);
		$select = $bdd->query("SELECT Titre FROM sujets WHERE Titre ='". $nomQCM."'");
		$nb_occ=0;
		foreach($select as $row) {
			if($row['Titre'] == $nomQCM) {
				$nb_occ++;
			}
		}
		if($nb_occ >=1){
			$_SESSION['nomQuestionnaire']=$nomQCM;
			$commande="sudo -u $utilisateur chmod 777 $dossierQCM".$nomQCM;
			exec($commande);
			header("Location:correction_csv.php");
		}
		else{
			echo "Le QCM n'existe pas, veuillez entrez le nom d'un questionnaire que vous avez créé.";
		}
	}
	if(isset($_POST['deco'])){		//bouton deconnexion
			session_destroy();
			header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
    <title>EASY TEST | CORRECTION</title>
    <link rel="stylesheet" href="style/style-pageUser.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>

    <header class="top">
        <nav class="navigation container">
            <a href="index.php" class="logo">EASY TEST</a>
            <ul class="nav-right">


				<form action='' method='post'>
					<input type="submit" id="bdeconnexion" name="deco" value="Déconnexion"/>
				</form>

						<li><a href="pageCompte.php">Compte</a></li>
						<li><a href="pageUtilisateur.php">Création QCM</a></li>
						 <li><a href="correction_nomQCM.php">Correction</a></li>
			</ul>
		</nav>
    </header>

<h1>Correction du QCM</h1>
			<center><strong>Indiquez le nom du questionnaire à corriger</strong></center>
			<br/><br/>
			  <form method="post" action="" enctype="multipart/form-data">
						<input type="submit" value="Vérification" name="verif">
<center><input type='text' name="nomQuestionnaire" placeholder="Nom du QCM à corriger" id="nomQuestionnaire" required> </center>  <!-- TITRE DU QUESTIONNAIRE -->

					</div>
			  </form>
<br/>
