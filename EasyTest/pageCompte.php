<?php
	session_start();
	if($_SESSION['verifConnexion'] != 1){
		header("Location:index.php");
	}
?>

<html>
<head>
   <meta charset="UTF-8">
    <title>EASY TEST | COMPTE</title>
    <link rel="stylesheet" href="style/style-pageCompte.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>

    <header class="top">
        <nav class="navigation container">
            <a href="#" class="logo">EASY TEST</a>
            <ul class="nav-right">


				<form action='' method='post'>
					<input type="submit" id="bdeconnexion" name="deco" value="Déconnexion"/>
				</form>

						<li><a href="pageUtilisateur.php">Création QCM</a></li>
						<li><a href="correction_nomQCM.php">Correction</a></li>



			</ul>
		</nav>
    </header>



	<center><div id='titre'>
		<h2>Consultez les informations de votre compte</h2>
	</div></center>

	<script type='text/javascript'>

		var boolMdp = true;

		function versPageUtil(){
			document.location.href = "pageUtilisateur.php";
		}

	</script>

</body>
</html>

<?php

        if(isset($_POST['deco'])){              //bouton deconnexion
	        session_destroy();
                header("Location:index.php");
         }
	include('fonctions.php');
	$bdd = bdd();

	//recherche des sujets créés par l'utilisateur
	$select = $bdd->query("SELECT Titre,Date_creation,Date_sujet,Duree,Nombre_exemplaire FROM sujets WHERE mail='".$_SESSION['mail']."'");
	$infosSujets="<table border = '1' cellpadding = '15'>
		<tr>
			<th>Nom du sujet</th><th>Date de création</th><th>Date de l'examen</th><th>Durée</th><th>Nombre d'exemplaire</th>
		</tr>";
        foreach($select as $row) {
		$infosSujets=$infosSujets."<tr>
		<td>".$row['Titre']."</td>
		<td>".$row['Date_creation']."</td>
		<td>".$row['Date_sujet']."</td>
		<td>".gmdate('H:i',$row['Duree']*60)."</td>
		<td>".$row['Nombre_exemplaire']."</td>
		</tr>";
	}

	//recherche des infos de l'utiliateur
	$infosUtilisateur= $bdd->query("SELECT * FROM utilisateurs WHERE mail='".$_SESSION['mail']."'");
	foreach($infosUtilisateur as $row) {
		$id=$row['id'];
	}

?>
	<!-- affichage des informations de l'utilisateur-->
	<div id='infoPerso'>
                <center><h3>Informations personnelles</h3></center>
                <br><br>
		<p>Identifiant : <?php echo $id; ?> </p>
		<p>Adresse mail : <?php echo $_SESSION['mail']; ?> </p>

	</div>

	<!-- affichage des sujets de l'utilisateur-->
	<div id='tableSujet'>
                <center><h3>Sujets précédemment créés</h3></center>
                <br><br>
                <?php echo $infosSujets; ?>
	</div>
