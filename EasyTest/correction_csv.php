<?php
	session_start();
	if($_SESSION['verifConnexion'] != 1){
		header("Location:index.php");
	}
	$nomQCM=$_SESSION['nomQuestionnaire'];
	//si le fichier ext correct, redirection vers la page d'insertion des scans
	if(isset($_POST['validation'])){
		if(isset($_SESSION['fichierCSV']) && !empty($_SESSION['fichierCSV']))
			header("Location:correction_scans.php");
		else	echo "<p>Veuillez insérer un fichier csv correct avant de poursuivre la correction</p>";
	}
	if(isset($_POST['deco'])){              //bouton deconnexion
	      session_destroy();
  			header("Location:index.php");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
    <title>EASY TEST | CORRECTION</title>
    <link rel="stylesheet" href="style/style-pageUser.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>

<body>
    <header class="top">
        <nav class="navigation container">
            <a href="index.php" class="logo">EASY TEST</a>
            <ul class="nav-right">


				<form action='' method='post'>
					<input type="submit" id="bdeconnexion" name="deco" value="Déconnexion"/>
				</form>
						<li><a href="pageUtilisateur.php">Création QCM</a></li>
						<li><a href="pageCompte.php">Compte</a></li>
						 <li><a href="correction_nomQCM.php">Correction</a></li>
			</ul>
		</nav>
    </header>

<h1>Correction du QCM</h1>
		<h2>Insertion du fichier csv contenant les identifiants des étudiants
		</h2><br/><br/><br/>
		<p><strong>Entrez le chemin absolu du fichier csv contenant les informations sur les étudiants, la première ligne du fichier étant: "Nom,Prenom,NumeroEtudiant"
		 (puis 1 ligne pour chaque étudiant du type nom,prenom,numero séparés par des virgules)</strong></p>
		 <form  method="post" action="" enctype="multipart/form-data">
			<!--<input type="text" name="fichier" > -->
			<input type="file" name="fichier" accept=".csv">
			<input type="submit" name="ok" value="Valider le fichier">
		</form>

		<!-- Si le fichier est correct, l'utilisateur peut valider les informations (dirigé dans correction_scans.php pour insérer les scans -->
		<form  method="post" action="">
			<center><input type="submit" name="validation" value="Valider les informations"></center>
		</form>
</body>
<?php
	require("parametres.php");
	//affiche les infos des etudiants dans le fichier csv
	function affichage($fic){
		echo"<p>";
		$ligne=0;
		echo "<table border='1' cellpadding='15'>";
		 while($tab=fgetcsv($fic,1024,',')){
			$ligne++;
			echo "<tr>";
			if($ligne==1){
				if($tab[0]!="Nom" || $tab[1]!="Prenom" || $tab[2]!="NumeroEtudiant"){
					echo "ERREUR: LA PREMIERE LIGNE DU FICHIER DOIT ÊTRE: Nom,Prenom,NumeroEtudiant";
					break;
				}
				echo "<th>Nom</th><th>Prénom</th><th>Numéro de l'étudiant</th>";
			}
			else{
				foreach($tab as $champs)
					echo "<td>$champs</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
		echo"</p>";
		return $ligne;
	}
	//envoi du fichier csv dans le dossier portant le nom du QCM contenu dans $dossierQCM et affichage des infos des étudiants
	if(isset($_POST['ok'])){
		$nomQCM=$_SESSION['nomQuestionnaire'];
		$uploaddir = $dossierQCM.$nomQCM;
		$uploadfile = $uploaddir.'/etudiants.csv';
		if(move_uploaded_file($_FILES['fichier']['tmp_name'], $uploadfile)) {
			echo 'Fichier envoyé';
		}
		else {
			echo 'Erreur lors de l\'envoi';
		}
		//$resultat =
		//print_r($resultat);
		//echo $resultat;
		/*
		$dossiersDuChemin=explode("/",$_POST['fichier']);
		$nomFichier=$dossiersDuChemin[count($dossiersDuChemin)-1];
		$commande="sudo -u $utilisateur chmod 666 ".$_POST['fichier'];
		exec($commande);
		$commande="sudo -u $utilisateur mv ".$_POST['fichier']." $dossierQCM".$nomQCM;
		exec($commande);
		*/
		//$fichier=$dossierQCM.$nomQCM."/".$nomFichier;
		$fichier = $uploadfile;
		if(file_exists($fichier)){
			$fic = fopen($fichier, "r");
			echo "<p>Voici les informations concernant les étudiants qui vont passer l'examen:</p>";
			$_SESSION['nombreEtudiantsQCM']=affichage($fic)-1;
			fclose($fic);
			$_SESSION['fichierCSV']=$fichier;
		}
		else	echo "<p>Fichier incorrect!</p>";
	}

?>
