<?php
	session_start();
	if($_SESSION['verifConnexion'] != 1){
		header("Location:index.php");
	}


	if(isset($_POST["valider"])){	//récupération et envoi des paramètres du QCM afin de créer les sujets et le corrigé dans la page traitement_QCM.php
		include('fonctions.php');
		$bdd = bdd();

		$_SESSION['nbExemplaire'] = $_POST['nbExemplaire'];
		$_SESSION['nbTotalQuestion'] = $_POST['nombreExemplaireTotal'];
		$_SESSION['date'] = $_POST['jour'].'/'.$_POST['mois'].'/'.$_POST['annee'];
		$_SESSION['datebd'] = $_POST['annee'].'-'.$_POST['mois'].'-'.$_POST['jour'];
		$_SESSION['duree'] = ($_POST['heureBBas'] * 60) + $_POST['minuteBBas'];
		//$_SESSION['nomQuestionnaire'] = $_POST['nomQuestionnaire'];
		$_SESSION['nomQuestionnaire'] = str_replace(' ', '_', $_POST['nomQuestionnaire']);
		$_SESSION['penalite'] = $_POST['penalite'];

		for($i = 1;$i<$_POST['nombreExemplaireTotal']+1;$i++){
			$temp = 'intitule'.$i;
			$temp2 = 'intituleQ'.$i;
			$_SESSION[$temp] = $_POST[$temp2];
			$bar='baremeQ'.$i;
			$_SESSION[$bar] = $_POST[$bar];

			for($y=1;$y<5;$y++){
				$temp = 'parRep'.$i.$y;
				$temp2 = 'rt'.$i.$y;
				$_SESSION[$temp] = $_POST[$temp2];

				$tmp = 'check'.$i.$y;
				$tmp2 = 'r'.$y.$i;
				$_SESSION[$tmp] = $_POST[$tmp2];
			}
		}
		//enregistrement du sujet dans la base de données
		$requete = $bdd->prepare('INSERT INTO sujets(Date_sujet, Duree, Nombre_exemplaire, Titre,mail) VALUES(?,?,?,?,?)');
		$requete->execute(array(htmlspecialchars($_SESSION['datebd']),htmlspecialchars($_SESSION['duree']),htmlspecialchars($_SESSION['nbExemplaire']),htmlspecialchars($_SESSION['nomQuestionnaire']),htmlspecialchars($_SESSION['mail'])));

		header('Location:traitement_QCM.php');
	}

	if(isset($_POST['deco'])){		//bouton deconnexion
		session_destroy();
		header("Location: index.php");
	}

?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
    <title>EASY TEST | CREATION DU QCM</title>
    <link rel="stylesheet" href="style/style-pageUser.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>

    <header class="top">
        <nav class="navigation container">
            <a href="index.php" class="logo">EASY TEST</a>
            <ul class="nav-right">


				<form action='' method='post'>
					<input type="submit" id="bdeconnexion" name="deco" value="Déconnexion"/>
				</form>
						<li><a href="pageCompte.php">Compte</a></li>
						 <li><a href="correction_nomQCM.php">Correction</a></li>
			</ul>
		</nav>
    </header>

		<fieldset>
			<legend> QCM </legend>

			<h3>Question 1</h3>
			<form action ="" method="post" name="question1" id="formulaire"> <!-- debut formulaire (contient questions et infos sur le sujet-->
				<input type='text' name="nomQuestionnaire" placeholder="nom du questionnaire" id="nomQuestionnaire" required><!-- TITRE DU QUESTIONNAIRE -->
				<input type='number'   id='penalite' name='penalite' placeholder="Pénalité en cas de mauvaise réponse" step="-1" min="-5" max="0"  required>


				<p><input id="intitule" type="textarea" name="intituleQ1" placeholder="intitulé de la question" required/>
					<input type='number'   id='baremeQ1' name='baremeQ1' placeholder="barème" step="1" min="1" max="20" required > </p>
					<p><input type="checkbox" name="r11"  id="check1"/> <label for="check1" > </label> <input id="reponse" type="text" name="rt11" placeholder="première réponse" required/></p>

				<p><input type="checkbox" name="r21" id="check2" /> <label for="check2" > </label> <input id="reponse" type="text" name="rt12" placeholder="deuxième réponse" required/> </p>
				<p><input type="checkbox" name="r31" id="check3" /> <label for="check3" > </label> <input id="reponse" type="text" name="rt13" placeholder="troisième réponse" required/> </p>
				<p><input type="checkbox" name="r41" id="check4"  /> <label for="check4" > </label> <input id="reponse" type="text" name="rt14" placeholder="quatrième réponse" required/> </p>
		</fieldset></br> </br></br>

				<button type="submit" name="valider" value="valider" id="validerBBas"> Valider </button>
				<input type='button' onclick='ajout_question()' name='ajouter' value='Ajouter une question' id='ajoutBBas'/>
				<input type='button' onclick='retirer_question()' name='retirer' value='Supprimer une question' id='retirerBBas' />
				<div id="BBas">
					<center>
						<label > <strong> Date de l'examen </strong> </label>
						<br/>
						<label for="jour" > <strong>Jour</strong></label>
						<input type='number'   id='jour' name='jour' min="1" max="31" value="1" required>
						<label for="mois"> <strong>Mois</strong> </label>
						<input type='number'  id='mois' name='mois' min="1" max="12" value="1" required>
						<label for="annee"> <strong>Année</strong> </label>
						<input type='number'  id='annee' name='annee' min="2017" value="2017"required>
						<label for="exemplaireBBas" > <strong>Nombre d'exemplaire </strong></label>
						<input type="number" name="nbExemplaire"  id='exemplaireBBas' min="1" max="100" value="1" required>
						</br></br>
						<label > <strong>Durée</strong> </label></br>
						<label for="heureBBas" > Heure</label>
						<input type='number'   id='heureBBas' name='heureBBas' value="0" min="0" max="3" >
						<label for="minutesBBas"> Minutes </label>
						<input type='number'  id='minutesBBas' name='minuteBBas' value="1" min="0" max="59"/>
					</center>
				</div>
				<input type='text' name="nombreExemplaireTotal" id='nombreDExemplaireTotal' value='1' style='display:none'/>	<!--input caché correspondant au nombre total de questions-->

		</form> <!-- Fin du formulaire-->


	<script type="text/javascript">
		var compteur = 2;
		var rajout;
		var compteurBBas = true;
		var memoireQuestion = [];

		function ecriture(){
			rajout = "";
			rajout += '<div id="ques'+compteur+'"><br>';
			rajout += '<h3>Question '+compteur+'</h3>';
			rajout += "<form action ='' method ='post' name='question"+compteur+"'>";
			rajout += "<p><input id='intitule' type='textarea' name='intituleQ"+compteur+"' placeholder='intitulé de la question' required/> <input type='number'   id='baremeQ"+compteur+"' name='baremeQ"+compteur+"' placeholder='barème' step='1' min='1' max='20'required /></p>" ;
			rajout += "<p><input type='checkbox' name='r1"+compteur+"' id='check1"+compteur+"'//> <label for='check1"+compteur+"'> </label><input id='reponse' type='text' name='rt"+compteur+"1' placeholder='première réponse' required/> </p>";
			rajout += "<p><input type='checkbox' name='r2"+compteur+"' id='check2"+compteur+"'//>  <label for='check2"+compteur+"'> </label> <input id='reponse' type='text' name='rt"+compteur+"2' placeholder='deuxième réponse' required/></p>";
			rajout += "<p><input type='checkbox' name='r3"+compteur+"' id='check3"+compteur+"'//>  <label for='check3"+compteur+"'> </label> <input id='reponse' type='text' name='rt"+compteur+"3' placeholder='troisième réponse' required/> </p>";
			rajout += "<p><input type='checkbox' name='r4"+compteur+"' id='check4"+compteur+"'//>  <label for='check4"+compteur+"'> </label> <input id='reponse' type='text' name='rt"+compteur+"4' placeholder='quatrième réponse' required/> </p>";
			rajout += "</form>"
		}

		function sauvEntrer(){ //sauvegarde les champs remplis par l'utilisateur
			i = 0;
			for (i = 0; i < 9*(compteur-1) ; i++){
				memoireQuestion[i] = document.getElementById("formulaire").elements[i].value;
			}
		}

		function ecritureEntrer(){ //ecrit les champs remplit par l'utilisateur
			for (i=0;i<9*(compteur-1);i++){
				document.getElementById("formulaire").elements[i].value = memoireQuestion[i];
			}
			memoireQuestion = [];
		}

		function ajout_question(){
			sauvEntrer();
			ecriture();
			document.getElementById('formulaire').innerHTML += rajout;
			ecritureEntrer();
			compteur++;
			document.getElementById('nombreDExemplaireTotal').value=compteur-1;
		}

		function retirer_question(){
			if(compteur>2){
				compteur--;
				var objAsupp = document.getElementById("ques"+compteur);
				objAsupp.parentNode.removeChild(objAsupp);
				document.getElementById('nombreDExemplaireTotal').value=compteur-1;
			}
			else{
				alert("Suppression impossible !");
			}
		}

		function cacherBBas(){
			if (compteurBBas == true){
				document.getElementById("cacheBBas").style = "background-image:url('images/fleche-Haut.jpg');";
				document.getElementById("BBas").style.display = "none";
				document.getElementById("cacheBBas").style.bottom = "100px";
				compteurBBas = !compteurBBas;
			}
			else if (compteurBBas == false){
				document.getElementById("cacheBBas").style = "background-image:url('images/fleche-Bas.jpg');";
				document.getElementById("BBas").style.display = "block";
				document.getElementById("cacheBBas").style.bottom = "100px";
				compteurBBas = !compteurBBas;
			}
		}

		function versPageAide(){
			document.location.href="aideQCM.php";
		}

		function versPageCompte(){
			document.location.href="pageCompte.php";
		}
		function versPageCorrection(){
			document.location.href="correction.php";
		}

	</script>


</body>
</html>
