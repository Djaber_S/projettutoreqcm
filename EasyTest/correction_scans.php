<?php
	session_start();
	if($_SESSION['verifConnexion'] != 1){
		header("Location:index.php");
	}
	$nomQCM=$_SESSION['nomQuestionnaire'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
    <title>EASY TEST | CORRECTION</title>
    <link rel="stylesheet" href="style/style-pageUser.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>

    <header class="top">
        <nav class="navigation container">
            <a href="index.php" class="logo">EASY TEST</a>
            <ul class="nav-right">


				<form action='' method='post'>
					<input type="submit" id="bdeconnexion" name="deco" value="Déconnexion"/>
				</form>
						<li><a href="pageUtilisateur.php">Création QCM</a></li>
						<li><a href="pageCompte.php">Compte</a></li>
						 <li><a href="correction_nomQCM.php">Correction</a></li>
			</ul>
		</nav>
    </header>

<h1>Correction du QCM</h1>
	<h2>Insertion des scans des copies</h2>
			<p>Maintenez la touche CTRL enfoncée pour envoyer plusieurs fichiers en même temps</p>
			  <form method="post" action="" enctype="multipart/form-data">
				<input name="uploads[]" type="file" multiple>
				<input type="submit" value="Envoyer les scans" name="envoyer">
			  </form>
</fieldset>
<?php 
	require("parametres.php");
	//envoi des scans dans le répertoire scans du dossier dont le nom est celui du questionnaire (cad le projet AMC)
	if(isset($_POST['envoyer'])) {
		$uploaddir = $dossierQCM.$nomQCM."/scans/";
		$i = 0;
		foreach ($_FILES['uploads'] as $file) {
		  $uploadfile = $uploaddir.basename($_FILES['uploads']['name'][$i]);
		  move_uploaded_file($_FILES['uploads']['tmp_name'][$i], $uploadfile);
		  $i++;
		}
		//on vérifie que l'utilisateur a bien rentré le bon nombre de scans pour corriger dans resultats.php
		$commande="sudo -u $utilisateur ls -l $dossierQCM"."$nomQCM/scans | wc -l";
		$output=shell_exec($commande);
		$nombreDeScansInseres=intval($output)-1;
		if($nombreDeScansInseres!=$_SESSION['nombreEtudiantsQCM']-1){
			if($_SESSION['nombreEtudiantsQCM']<=0)	echo "<p>Aucun étudiant dans votre fichier csv, veuillez le modifier</p>";
			else	echo "<p>Vous avez indiqué ".$_SESSION['nombreEtudiantsQCM']." étudiants dans le fichier csv mais vous avez inséré ".$nombreDeScansInseres." scans dans le dossier ".$dossierQCM.$nomQCM."/scans/, veuillez modifier le fichier csv et/ou insérer d'autres scans de copies en PDF</p>";
		}
		else	header("Location:resultats.php");
	}
	if(isset($_POST['deco'])){              //bouton deconnexion
	        session_destroy();
                header("Location:index.php");
         }
?>
