<?php
	session_start();
//Connexion à la base de donnée

	include('fonctions.php');
	$bdd = bdd();



if(isset($_POST['boutonLog'])){
		$select = $bdd->query('SELECT * FROM utilisateurs');
		foreach($select as $row) {
			if($row['id'] == $_POST['id'] && $row['password'] == sha1($_POST['pwd'])) {

				$_SESSION['mail'] = $row['mail'];

				$_SESSION['verifConnexion'] = 1;
				header('Location:pageUtilisateur.php');
			}
		}
}



?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>EASY | Connexion</title>
    <link rel="stylesheet" href="style/style-identification.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>

<body>

    <header class="top">
        <nav class="navigation container">
            <a href="index.php" class="logo">EASY TEST</a>
            <ul class="nav-right">
                <li><a href="connexion.php">Se connecter</a></li>
                <li><a href="inscription.php">S'inscrire</a></li>
            </ul>
        </nav>
    </header>





<div class="container">
        <div class="account-page">
            <div class="form">
                <h1>Connexion</h1>

                <form class='form_inscription' method="post" action="" name="connexion">
                    <input type="text" name="id" placeholder="login" />
                    <input type="password" name="pwd" placeholder="Mot de passe" />
                    <button type="submit" name="boutonLog" value="connexion" >Connexion</button>
                    <p class="message">Pas Inscrit ? <a href="inscription.php">Inscrivez vous!</a></p>
                </form>
				<a href="mdp_oublier.php" style="position:absolute;right:10px;">mot de passe oublié</a>
            </div>
        </div>
    </div>


    <footer class="footer">
        <div class="container">
            <p><small>&copy; Université de Versailles Saint Quentin en Yvelines 2017 France</small></p>
        </div>
    </footer>
</body>
</html>
