<?php
	session_start();
	if($_SESSION['verifConnexion'] != 1){
		header("Location:index.php");
	}
	$nomQCM=$_SESSION['nomQuestionnaire'];
	$malus=$_SESSION['penalite'];

		///////////////////////////Variables///////////////////////////////////////////////////////////
	require("parametres.php");
	$commande="mkdir $dossierQCM"; //creation du répertoire contenant les projets AMC s'il n'existe pas
	exec($commande);

	$filename=$dossierQCM.$nomQCM.".tex";	//Nom du fichier
	$commande="chmod 777 -R $dossierQCM";
	exec($commande);
	$nbExemplaire=$_SESSION['nbExemplaire'];
	$date=$_SESSION['date'];
	$duree=$_SESSION['duree'];
	$nbQuestion=$_SESSION['nbTotalQuestion'];

/////////////////////////////Fichier LaTex/////////////////////////////////////////////////

	$fic=fopen($filename,"a+");
	$commande="chmod 777 $filename";
	exec($commande);
	//ENTETE
	$entete="\documentclass[a4paper]{article}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[francais,bloc,completemulti]{automultiplechoice}
\begin{document}

\\exemplaire{".$nbExemplaire."}{

%%% debut de l'en-tête des copies :

\\noindent{\bf QCM  \hfill TEST}

\\vspace*{.5cm}
\begin{minipage}{.4\linewidth}
\centering\large\bf Test\\ Examen du ".$date."\\end{minipage}
\champnom{\\fbox{
	        \begin{minipage}{.5\linewidth}
	          Nom et prénom :

	          \\vspace*{.5cm}\dotfill
	          \\vspace*{1mm}
	        \\end{minipage}
	 }}

\begin{center}\\em
Durée : ".$duree." minutes.

  Aucun document n'est autorisé.
  L'usage de la calculatrice est interdit.

  Les questions faisant apparaître le symbole \multiSymbole{} peuvent
  présenter zéro, une ou plusieurs bonnes réponses. Les autres ont
  une unique bonne réponse.

  Des points négatifs pourront être affectés à de \\emph{très
    mauvaises} réponses.

\\end{center}
\\vspace{1ex}
{\setlength{\parindent}{0pt}\hspace
*
{\\fill}\AMCcode{NumeroEtudiant}{8}\hspace
*
{\\fill}
\begin{minipage}[b]{6.5cm}
$\longleftarrow{}$\hspace{0pt plus 1cm} Veuillez inscrire votre numéro d’étu\-diant.
\\vspace{3ex}
\hfill\\vspace{5ex}\\end{minipage}\hspace
*
{\\fill}
}
\\vspace{1ex}


";//FIN ENTETE
	$fin="
% \AMCaddpagesto{3}
}
\\end{document}"; //fin fichier tex

fwrite($fic,$entete); // ECRITURE DE L'ENTETE DANS LE FICHIER LATEX
	////////////////////////////Questions/////////////////////////////////////////////////
	for ($i=0; $i<$nbQuestion; $i++){
		$y=$i+1;
		$intitule=$_SESSION['intitule'.$y];
		$rep1=$_SESSION['parRep'.$y.'1'];
		$rep2=$_SESSION['parRep'.$y.'2'];
		$rep3=$_SESSION['parRep'.$y.'3'];
		$rep4=$_SESSION['parRep'.$y.'4'];
		$check1=$_SESSION['check'.$y.'1'];
		$check2=$_SESSION['check'.$y.'2'];
		$check3=$_SESSION['check'.$y.'3'];
		$check4=$_SESSION['check'.$y.'4'];
		$bareme=$_SESSION['baremeQ'.$y];
		$checkRep1="";
		$checkRep2="";
		$checkRep3="";
		$checkRep4="";
		$cptRep=0;
		$nbRep="";
		$question="";
		if ($check1 == 'on'){
			$checkRep1="\bonne{".$rep1."}";
			$cptRep++;
		}
		else{
			$checkRep1="\mauvaise{".$rep1."}";
		}
		if ($check2 == 'on'){
			$checkRep2="\bonne{".$rep2."}";
			$cptRep++;
		}
		else{
			$checkRep2="\mauvaise{".$rep2."}";
		}
		if ($check3 == 'on'){
			$checkRep3="\bonne{".$rep3."}";
			$cptRep++;
		}
		else{
			$checkRep3="\mauvaise{".$rep3."}";
		}
		if ($check4 == 'on'){
			$checkRep4="\bonne{".$rep4."}";
			$cptRep++;
		}
		else{
			$checkRep4="\mauvaise{".$rep4."}";
		}

		if ($cptRep>1){
			$nbRep="
\begin{questionmult}{".$y."}";
			$question=$nbRep.$intitule."\\bareme{formula=(NBC==NB)&&(NMC==0)?".$bareme.":".$malus."*NMC+(".$bareme."/NB)*NBC}
\begin{reponses}".$checkRep1.$checkRep2.$checkRep3.$checkRep4.
"\\end{reponses}\\end{questionmult}

";
		}
		else{
			$nbRep="
\begin{question}{".$y."}";
			$question=$nbRep.$intitule."\\bareme{b=".$bareme.",m=".$malus."}
\begin{reponses}".$checkRep1.$checkRep2.$checkRep3.$checkRep4.
"\\end{reponses}\\end{question}

";
		}
		//ON ECRIT LES QUESTIONS DANS LE FICHIER LATEX,PUIS ON LE FERME
		fwrite($fic,$question);

	}//for
	fwrite($fic,$fin);
	fclose($fic);


	//CREATION PROJET AMC ET DES FICHIERS

	$commande="mkdir -p ".$dossierQCM.$nomQCM."/cr/corrections/jpg";
	exec($commande);
	$commande="mkdir ".$dossierQCM.$nomQCM."/cr/corrections/pdf";
	exec($commande);
	$commande="mkdir ".$dossierQCM.$nomQCM."/cr/diagnostic";
	exec($commande);
	$commande="mkdir ".$dossierQCM.$nomQCM."/cr/zooms";
	exec($commande);
	$commande="mkdir ".$dossierQCM.$nomQCM."/scans";
	exec($commande);
	$commande="mkdir ".$dossierQCM.$nomQCM."/data";
	exec($commande);
	$commande="mkdir ".$dossierQCM.$nomQCM."/exports";
	exec($commande);
	$commande="mkdir ".$dossierQCM.$nomQCM."/copies";
	exec($commande);
	$commande="chmod 777 -R $dossierQCM";
	exec($commande);
	$commande="sudo -u $utilisateur auto-multiple-choice prepare --mode s --prefix $dossierQCM"."$nomQCM $filename --out-sujet sujet.pdf --out-corrige corrige.pdf --out-calage calage.xy"; // creation sujets, corrigé et calage
	exec($commande);

	//déplacement du fichier latex, des pdf et du fichier de calage vers le dossier du projet AMC (nommé le nom du QCM)
	$commande="sudo -u $utilisateur mv $dossierQCM"."sujet.pdf $dossierQCM"."$nomQCM";
	exec($commande);
	$commande="sudo -u $utilisateur mv $dossierQCM"."corrige.pdf $dossierQCM"."$nomQCM";
	exec($commande);
	$commande="sudo -u $utilisateur mv $dossierQCM"."calage.xy $dossierQCM"."$nomQCM";
	exec($commande);
	$commande="sudo -u $utilisateur mv $filename $dossierQCM"."$nomQCM";
	exec($commande);

	//ouverture du sujet
	$full_path = $dossierQCM.$nomQCM."/sujet.pdf";
	$file_name = basename($full_path);

	ini_set('zlib.output_compression', 0);
	$date = gmdate(DATE_RFC1123);

	header('Pragma: public');
	header('Cache-Control: must-revalidate, pre-check=0, post-check=0, max-age=0');

	header('Content-Tranfer-Encoding: none');
	header('Content-Length: '.filesize($full_path));
	header('Content-MD5: '.base64_encode(md5_file($full_path)));
	header('Content-Type: application/octetstream; name="'.$file_name.'"');
	header('Content-Disposition: attachment; filename="'.$file_name.'"');

	header('Date: '.$date);
	header('Expires: '.gmdate(DATE_RFC1123, time()+1));
	header('Last-Modified: '.gmdate(DATE_RFC1123, filemtime($full_path)));

	readfile($full_path);
?>
