<?php
session_start();

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title> EASY TEST</title>
    <link rel="stylesheet" href="style/index.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>

<body background="images/fond.png";background-repeat="no-repeat";background-position="bottom right";>

    <header class="top">
	
        <nav class="navigation container">
              <a href="#" class="logo">EASY TEST</a>
            <ul class="nav-right">
                <li><a href="connexion.php">Se connecter</a></li>
                <li><a href="inscription.php">S'inscrire</a></li>
            </ul>
        </nav>
    </header>

    <div class="hero" 	>

        <div class="container">
            <h1 class="titre">Easy Test </h1>
            <p class="description">Téléchargez des sujets de QCM en PDF rapidement et simplement <br/>et corrigez les copies des étudiants avec Easy Test !</p>
        </div>
    </div></br> </br></br>

    

    <footer class="footer">
        <div class="container">
            <p><small>&copy; 2017 Université de Versailles Saint Quentin en Yvelines France</small></p>
        </div>
    </footer>

</body>
</html>
